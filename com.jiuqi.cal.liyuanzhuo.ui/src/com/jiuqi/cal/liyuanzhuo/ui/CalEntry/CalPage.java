package com.jiuqi.cal.liyuanzhuo.ui.CalEntry;

import com.jiuqi.dna.ui.common.constants.JWT;
import com.jiuqi.dna.ui.wt.layouts.GridData;
import com.jiuqi.dna.ui.wt.layouts.GridLayout;
import com.jiuqi.dna.ui.wt.widgets.Button;
import com.jiuqi.dna.ui.wt.widgets.Composite;
import com.jiuqi.dna.ui.wt.widgets.Page;
import com.jiuqi.dna.ui.wt.widgets.Text;
import com.jiuqi.dna.ui.wt.widgets.ToolBar;
import com.jiuqi.dna.ui.wt.widgets.ToolItem;
/**
 * 计算器界面
 * @author lsz
 *
 */
public class CalPage extends Page {

	// 计算器文本
	private String[] texts = new String[] { "MC", "MR", "MS", "M+", "M-", "←", "CE", "C", "±", "√", "7", "8", "9", "/",
			"%", "4", "5", "6", "*", "1/x", "1", "2", "3", "-", "=", "0", ".", "+" };

	public CalPage(Composite parent) {
		super(parent);
		// new一个GridLayout对象
		GridLayout gridLayout = new GridLayout();
		// new一个GridData并设置高度和宽度
		GridData layoutData = new GridData(500, 400);
		// 为页面容器设置GridLayout
		this.setLayout(gridLayout);
		// 为页面容器设置GridData
		this.setLayoutData(layoutData);

		// 新建一个ToolBar控件其属于页面容器
		ToolBar toolBar = new ToolBar(this);
		// new一个GridData对象
		GridData toolBarData = new GridData();
		// 设置水平对齐以及填满父容器
		toolBarData.horizontalAlignment = JWT.FILL;
		toolBarData.grabExcessHorizontalSpace = true;
		// 为ToolBar设置GridData
		toolBar.setLayoutData(toolBarData);

		// 新建三个ToolItem属于ToolBar并设置标题文字
		ToolItem toolItem1 = new ToolItem(toolBar);
		toolItem1.setText("查看(V)");
		ToolItem toolItem2 = new ToolItem(toolBar);
		toolItem2.setText("编辑(E)");
		ToolItem toolItem3 = new ToolItem(toolBar);
		toolItem3.setText("帮助(H)");

		// 新建容器
		Composite c = new Composite(this);
		// 为容器控件设置GridLayout
		c.setLayout(new GridLayout());
		// 新建文本控件属于c容器并设定文本框中的文字居右显示
		Text t = new Text(c, JWT.RIGHT);
		// 设置文本
		t.setText("0.0");
		// 设置文本控件填满父容器
		GridData textData = new GridData(GridData.FILL_BOTH);
		textData.widthHint=318;
		textData.heightHint=100;
		t.setLayoutData(textData);
		// 新建容器
		Composite d = new Composite(this);
		// 设置网格容器5列
		d.setLayout(new GridLayout(5));
		// 循环为button设置文本
		// "="button设置为占两行，"0"button设置为占两列
		// 并分别设置button的长度和高度
		for (int i = 0; i < texts.length; i++) {
			Button button = new Button(d);
			button.setText(texts[i]);
			if (texts[i] == "=") {
				GridData data = new GridData();
				data.verticalSpan = 2;
				data.widthHint = 60;
				data.heightHint = 120;
				button.setLayoutData(data);
			} else if (texts[i] == "0") {
				GridData data = new GridData();
				data.horizontalSpan = 2;
				data.widthHint = 120;
				data.heightHint = 60;
				button.setLayoutData(data);
			} else {
				GridData Commondata = new GridData();
				Commondata.widthHint = 60;
				Commondata.heightHint = 60;
				button.setLayoutData(Commondata);
			}
		}
	}

}
