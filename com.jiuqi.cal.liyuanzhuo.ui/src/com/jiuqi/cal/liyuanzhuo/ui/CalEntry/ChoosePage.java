package com.jiuqi.cal.liyuanzhuo.ui.CalEntry;

import com.jiuqi.dna.bap.common.control.SearchBar;
import com.jiuqi.dna.ui.common.constants.JWT;

import com.jiuqi.dna.ui.custom.combo.ComboList;
import com.jiuqi.dna.ui.wt.graphics.CBorder;
import com.jiuqi.dna.ui.wt.graphics.Color;
import com.jiuqi.dna.ui.wt.layouts.FillLayout;
import com.jiuqi.dna.ui.wt.layouts.GridData;
import com.jiuqi.dna.ui.wt.layouts.GridLayout;
import com.jiuqi.dna.ui.wt.widgets.Button;
import com.jiuqi.dna.ui.wt.widgets.Composite;
import com.jiuqi.dna.ui.wt.widgets.Label;
import com.jiuqi.dna.ui.wt.widgets.List;
import com.jiuqi.dna.ui.wt.widgets.Page;

public class ChoosePage extends Page{
	// �����߿�
	private final CBorder cborder = new CBorder(1, JWT.BORDER_STYLE_SOLID, Color.COLOR_SKYBLUE.getColor());

	public ChoosePage(Composite parent) {
		super(parent);
		this.setLayout(new GridLayout());
		this.setLayoutData(GridData.INS_FILL_BOTH);
		this.setBorder(cborder);

		Composite comMain = new Composite(this);
		comMain.setLayout(new GridLayout());
		GridData comMainGridData = new GridData();
		comMainGridData.horizontalAlignment = JWT.CENTER;
		comMainGridData.grabExcessHorizontalSpace = true;
		comMainGridData.verticalAlignment = JWT.CENTER;
		comMainGridData.grabExcessVerticalSpace = true;
		comMainGridData.widthHint = 735;
		comMainGridData.heightHint = 575;
		comMain.setLayoutData(comMainGridData);
		comMain.setBorder(cborder);
		initMian(comMain);
	}

	private void initMian(Composite comMain) {
		// ����������
		Composite comTop = new Composite(comMain);
		comTop.setLayout(new GridLayout(2));
		GridData comTopGridData = new GridData();
		comTopGridData.horizontalAlignment = JWT.FILL;
		comTopGridData.grabExcessHorizontalSpace = true;
		comTopGridData.verticalIndent = 15;
		comTop.setLayoutData(comTopGridData);
		Label label1 = new Label(comTop);
		label1.setText("�����ƣ�");
		// �����б�
		ComboList comboList = new ComboList(comTop);
		comboList.setLayoutData(GridData.INS_FILL_HORIZONTAL);

		// �ֶ�����
		Composite comContMain = new Composite(comMain);
		comContMain.setLayout(new GridLayout(3));
		GridData comContMainGridData = new GridData();
		comContMainGridData.horizontalAlignment = JWT.FILL;
		comContMainGridData.grabExcessHorizontalSpace = true;
		comContMainGridData.verticalAlignment = JWT.FILL;
		comContMainGridData.grabExcessVerticalSpace = true;
		comContMainGridData.verticalIndent = 10;
		comContMain.setLayoutData(comContMainGridData);

		// �������
		Composite comLeft = new Composite(comContMain);
		comLeft.setLayout(new GridLayout());
		GridData comLeftGrid = new GridData();
		comLeftGrid.widthHint = 320;
		comLeftGrid.verticalAlignment = JWT.FILL;
		comLeftGrid.grabExcessVerticalSpace = true;
		comLeft.setLayoutData(comLeftGrid);

		Label label2 = new Label(comLeft);
		label2.setText("�����ֶΣ�");
		// ���������
		Composite searchComL = new Composite(comLeft);
		searchComL.setLayout(new FillLayout());
		GridData searchComGridData = new GridData();
		searchComGridData.horizontalAlignment = JWT.FILL;
		searchComGridData.grabExcessHorizontalSpace = true;
		searchComGridData.verticalIndent = 10;
		searchComL.setLayoutData(searchComGridData);
		SearchBar searchBarLeft = new SearchBar(searchComL);

		GridData listGrid = new GridData();
		listGrid.widthHint = 320;
		listGrid.heightHint = 460;
		listGrid.verticalIndent = 6;
		// ����б��ؼ�
		List listLeft = new List(comLeft);
		listLeft.setLayoutData(listGrid);

		// �м�����
		Composite comCenter = new Composite(comContMain); 
		comCenter.setLayout(new GridLayout());
		GridData comCenterGrid = new GridData();
		comCenterGrid.widthHint = 85;
		comCenterGrid.verticalAlignment = JWT.CENTER;
		comCenterGrid.grabExcessVerticalSpace = true;
		comCenter.setLayoutData(comCenterGrid);

		Button butChoose = new Button(comCenter);
		butChoose.setLayoutData(GridData.INS_FILL_HORIZONTAL);
		butChoose.setText("ѡ��");
	

		Button butAllChoose = new Button(comCenter);
		butAllChoose.setLayoutData(GridData.INS_FILL_HORIZONTAL);
		butAllChoose.setText("ȫѡ");

		Button butDelete = new Button(comCenter);
		butDelete.setLayoutData(GridData.INS_FILL_HORIZONTAL);
		butDelete.setText("ɾ��");

		Button butAllDelete = new Button(comCenter);
		butAllDelete.setLayoutData(GridData.INS_FILL_HORIZONTAL);
		butAllDelete.setText("ȫɾ");

		// �ұ�����
		Composite comReght = new Composite(comContMain);
		comReght.setLayout(new GridLayout());
		GridData comReghtGrid = new GridData();
		comReghtGrid.widthHint = 320;
		comReghtGrid.verticalAlignment = JWT.FILL;
		comReghtGrid.grabExcessVerticalSpace = true;
		comReght.setLayoutData(comReghtGrid);

		Label label3 = new Label(comReght);
		label3.setText("��ѡ�ֶΣ�");
		// �ұ�������
		Composite searchComR = new Composite(comReght);
		searchComR.setLayout(new FillLayout());
		searchComR.setLayoutData(searchComGridData);
		SearchBar searchBarReght = new SearchBar(searchComR);

		// �ұ��б��ؼ�
		List listReght = new List(comReght);
		listReght.setLayoutData(listGrid);
	}
}
