package com.jiuqi.cal.liyuanzhuo.ui.car;



import java.awt.Checkbox;
import java.util.List;

import com.jiuqi.carinfo.ui.tree.CarTreeViewerProvider.CarTreeViewerProvider;
import com.jiuqi.carinfo.ui.tree.CarTreeViewerCompare.CarTreeViewerCompare;
import com.jiuqi.carInfo.liyuanzhuo.entity.FCarInfo;
import com.jiuqi.carInfo.liyuanzhuo.resultType.CarInfoListResultType;
import com.jiuqi.carInfo.liyuanzhuo.storage.carInfoListQuery;
import com.jiuqi.carInfo.liyuanzhuo.task.CreateCarInfoTask;
import com.jiuqi.carInfo.liyuanzhuo.task.DelCarInfoTask;
import com.jiuqi.dna.bap.common.constants.BapImages;
import com.jiuqi.dna.bap.common.control.SearchBar;
import com.jiuqi.dna.bap.common.paginate.PagerBar;
import com.jiuqi.dna.bap.common.paginate.Paginate;
import com.jiuqi.dna.bap.common.paginate.ResourceFinder;
import com.jiuqi.dna.bap.common.paginate.render.PaginateRender;
import com.jiuqi.dna.core.type.GUID;
import com.jiuqi.dna.ui.common.constants.JWT;
import com.jiuqi.dna.ui.custom.Option;
import com.jiuqi.dna.ui.custom.combo.DatePicker;
import com.jiuqi.dna.ui.custom.configure.editors.CheckboxGroupSelector;
import com.jiuqi.dna.ui.wt.events.ActionEvent;
import com.jiuqi.dna.ui.wt.events.ActionListener;
import com.jiuqi.dna.ui.wt.events.GridCheckEvent;
import com.jiuqi.dna.ui.wt.events.GridCheckListener;
import com.jiuqi.dna.ui.wt.events.SelectionEvent;
import com.jiuqi.dna.ui.wt.events.SelectionListener;
import com.jiuqi.dna.ui.wt.graphics.CBorder;
import com.jiuqi.dna.ui.wt.graphics.Color;
import com.jiuqi.dna.ui.wt.graphics.ImageDescriptor;
import com.jiuqi.dna.ui.wt.grid2.Grid2;
import com.jiuqi.dna.ui.wt.grid2.GridCell;
import com.jiuqi.dna.ui.wt.grid2.GridModel;
import com.jiuqi.dna.ui.wt.grid2.grid2data.GridCellData;
import com.jiuqi.dna.ui.wt.layouts.FillLayout;
import com.jiuqi.dna.ui.wt.layouts.GridData;
import com.jiuqi.dna.ui.wt.layouts.GridLayout;
import com.jiuqi.dna.ui.wt.viewers.TreeViewer;
import com.jiuqi.dna.ui.wt.widgets.Button;
import com.jiuqi.dna.ui.wt.widgets.Composite;
import com.jiuqi.dna.ui.wt.widgets.Label;
import com.jiuqi.dna.ui.wt.widgets.Page;
import com.jiuqi.dna.ui.wt.widgets.SashForm;
import com.jiuqi.dna.ui.wt.widgets.Text;
import com.jiuqi.dna.ui.wt.widgets.ToolBar;
import com.jiuqi.dna.ui.wt.widgets.ToolItem;

public class CarAddPage extends Page{

	// 创建边框
	private final CBorder cborder = new CBorder(1, JWT.BORDER_STYLE_SOLID, Color.COLOR_SKYBLUE.getColor());

	private Text carNumText = null;
	private GridCellData carNumCell = null;
	private Paginate<FCarInfo> paginate;
	private PagerBar pagerBar;
//	private FCarInfo saveCarInfo = null;
	public CarAddPage(Composite parent) {
		super(parent);


		// 为页面容器设置GridLayout
		this.setLayout(new FillLayout());
		// 为页面容器设置GridData
		this.setLayoutData(GridData.INS_FILL_BOTH);
		//设置边框
		this.setBorder(cborder);
		
		Composite comMain = new Composite(this);
		comMain.setLayout(new GridLayout());
		GridData comMainGridData = new GridData();
		comMain.setLayoutData(GridData.INS_FILL_HORIZONTAL);
		comMain.setBorder(cborder);
		createToolBar(comMain);
		createSencodCom(comMain);

	}
	
	/**
	 * 创建工具栏
	 * @param comMain
	 */
	public void createToolBar(Composite comMain) {
		
		// 工具栏容器
		Composite comToolBar = new Composite(comMain);
		comToolBar.setLayout(new GridLayout());
		GridData comCenterGrid = new GridData();
		comToolBar.setLayoutData(GridData.INS_FILL_HORIZONTAL);
		comToolBar.setBorder(cborder);
//		comToolBar.setBackground(Color.);
		//创建工具栏
		ToolBar toolbar = new ToolBar(comToolBar);
		//创建工具栏子控件，并放到工具栏
		ToolItem addItem = new ToolItem(toolbar);
		
		//新建按钮
		ImageDescriptor addImage = getContext().find(ImageDescriptor.class, BapImages.ico_add);
		//设置工具栏图标
		addItem.setImage(addImage);
		//设置显示名称
		addItem.setText("新建");
		addItem.setSelection(true);
		
		
		//创建工具栏子控件，并放到工具栏
		ToolItem updateItem = new ToolItem(toolbar);
		//修改按钮
		ImageDescriptor updateImage = getContext().find(ImageDescriptor.class, BapImages.ico_modify);
		//设置工具栏图标
		updateItem.setImage(updateImage);
		//设置显示名称
		updateItem.setText("修改");
		
		//创建工具栏子控件，并放到工具栏
		ToolItem saveItem = new ToolItem(toolbar);
		//保存按钮
		ImageDescriptor saveImage = getContext().find(ImageDescriptor.class, BapImages.ico_save);
		//设置工具栏图标
		saveItem.setImage(saveImage);
		//设置显示名称
		saveItem.setText("保存");
		//添加action
		saveItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String carNum = carNumText.getText();
				System.out.println("获取到输入框的值："+carNum);
//				System.out.println("测试" + saveCarInfo.getCarName());
				CreateCarInfoTask task = new CreateCarInfoTask();
				task.setCarNum(carNum);
//				task.setCarName(saveCarInfo.getCarName());
				getContext().handle(task);
			}
		});
		
		//创建工具栏子控件，并放到工具栏
		ToolItem delItem = new ToolItem(toolbar);
		//删除按钮
		ImageDescriptor delImage = getContext().find(ImageDescriptor.class, BapImages.ico_delete);
		//设置工具栏图标
		delItem.setImage(delImage);
		//设置显示名称
		delItem.setText("删除");
		//添加删除操作
		delItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
//				System.out.println("选中的车辆编码值为"+carNumCell.getDataExString());
				DelCarInfoTask task = new DelCarInfoTask();
//				task.setCarNum(carNumCell.getDataExString());
				task.setCarNum("11");
				getContext().handle(task);
			}
		});
		
		//创建工具栏子控件，并放到工具栏
		ToolItem closeItem = new ToolItem(toolbar);
		//关闭按钮
		ImageDescriptor closeImage = getContext().find(ImageDescriptor.class, BapImages.ico_close_window);
		//设置工具栏图标
		closeItem.setImage(closeImage);
		//设置显示名称
		closeItem.setText("关闭");
	}
	
	/**
	 * 创建工具栏容器下面的容器
	 * 在这个容器中实现 车辆信息添加布局
	 * @param comMain
	 */
	public void  createSencodCom(Composite comMain) {
		// 创建容器
		Composite secondCom = new Composite(comMain);
		secondCom.setLayout(new FillLayout());
		GridData comReghtGrid = new GridData();
		comReghtGrid.widthHint = 1980;
		comReghtGrid.verticalAlignment = JWT.FILL;
		comReghtGrid.grabExcessVerticalSpace = true;
		secondCom.setLayoutData(comReghtGrid);
		secondCom.setBorder(cborder);
		//左右样式
		SashForm sashFormOne = new SashForm(secondCom);
		//设置左右比例：
		sashFormOne.setWeights("1:3");
		sashFormOne.setBorder(cborder);
		sashFormOne.getFirstComposite();
		
		//创建左侧树容器
		Composite firstComposite = sashFormOne.getFirstComposite();
		firstComposite.setLayout(new FillLayout());
		firstComposite.setLayoutData(GridData.INS_FILL_HORIZONTAL);
		createTreeCom(firstComposite);
		
		//右侧容器，放置列表与新增信息框
		Composite secondComposite = sashFormOne.getSecondComposite();
		secondComposite.setLayout(new FillLayout());
		secondComposite.setLayoutData(GridData.INS_FILL_HORIZONTAL);
		//上下样式
		SashForm sashFormTwo = new SashForm(secondComposite,JWT.VERTICAL );
		//设置两个容器
		Composite firstComTop = sashFormTwo.getFirstComposite(); 
		firstComTop.setBorder(cborder);
		//列表展示部分
		Composite secondComUp = sashFormTwo.getFirstComposite();
		secondComUp.setBorder(cborder);
		GridLayout gridList = new GridLayout();
		gridList.marginLeft=100;
		gridList.marginTop=20;
		secondComUp.setLayout(gridList);
		GridData ListGridData = new GridData();
		
		ListGridData.verticalAlignment = JWT.CENTER;
		ListGridData.grabExcessVerticalSpace = true;
		secondComUp.setLayoutData(ListGridData);
		//创建表格内容
		createListCom(secondComUp);
		
		// 新增页面容器
		Composite secondComDown = sashFormTwo.getSecondComposite();
		createCarAddCom(secondComDown);
		
		
	}
	
	/**
	 * 创建左侧树
	 */
	public void createTreeCom(Composite treeCom){
		final TreeViewer treeViewer = new TreeViewer(treeCom);
		//实例化一个“内容和标签提供器”
		CarTreeViewerProvider carTreeViewerProvider = new CarTreeViewerProvider(getContext());
		//实例化一个“元素比较器”
		CarTreeViewerCompare carTreeViewerCompare = new CarTreeViewerCompare();
		//设置TreeViewer
		treeViewer.setContentProvider(carTreeViewerProvider);
		treeViewer.setLabelProvider(carTreeViewerProvider);
		treeViewer.setComparer(carTreeViewerCompare);
		
		treeViewer.setInput(null);
		
//		FCarInfo carInfo = getContext().get(FCarInfo.class);
//		treeViewer.setInput(carInfo);
//		treeViewer.addSelectionListener(new SelectionListener() {
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//				//通过getSelection()方法获取节点元素并转换成FCarInfo类型
//				FCarInfo carInfo = (FCarInfo) treeViewer.getSelection();
//				}
//		});
	}
	
	
	/**
	 * 创建列表容器
	 * @param secondComUp
	 */
	
	public void createListCom(Composite secondComUp) {
		//创建搜索框
		Composite searchComL = new Composite(secondComUp);
		searchComL.setLayout(new FillLayout());
		searchComL.setBorder(cborder);
		GridData searchComGridData = new GridData();
		searchComGridData.widthHint=200;
		searchComGridData.grabExcessHorizontalSpace = true;
		searchComGridData.verticalIndent = 10;
		searchComL.setLayoutData(searchComGridData);
		SearchBar searchBarLeft = new SearchBar(searchComL);
		
		//创建表格
		final Grid2 grid2 = new Grid2(secondComUp);
		grid2.setActiveBorder(cborder);
		
		GridModel gridModel = grid2.getModel();
		gridModel.setColumnCount(5);
		gridModel.setRowCount(12);
		gridModel.setHeaderColumnCount(5);
		gridModel.setHeaderRowCount(10);
		GridCell row1_col1 = gridModel.getGridCell(0, 0);
		GridCell row1_col2 = gridModel.getGridCell(1, 0);
		GridCell row1_col3 = gridModel.getGridCell(2, 0);
		GridCell row1_col4 = gridModel.getGridCell(3, 0);
		GridCell row1_col5 = gridModel.getGridCell(4, 0);
		row1_col1.setShowText("序号");
		row1_col2.setShowText("车辆编码");
		row1_col3.setShowText("车辆名称");
		row1_col4.setShowText("车辆类型");
		row1_col5.setShowText("购买日期");
		//数据行
		try {
			GridCell data1 = gridModel.getGridCell(1, 1);	
			data1.setData(new CarInfoListResultType());
			
			List<CarInfoListResultType> list = getContext().getList(CarInfoListResultType.class);
			GridModel gridModel2 = grid2.getModel();
			gridModel2.setRowCount(list .size()+1);
			for(int i=0;i<list.size();i++){
				CarInfoListResultType info = list.get(i);
			     GridCell cell_1 = grid2.getModel().getGridCell(1, i+1);
			     cell_1.setShowText(info.getCarNum());
//			     carNumCell.setCellData("carNum", info.getCarName());
			}
		} catch (UnsupportedOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//给表格添加选中监听事件
		grid2.addCheckListener(new GridCheckListener() {
			
			@Override
			public void cellStateChanged(GridCheckEvent check) {
				// TODO Auto-generated method stub
				GridCell cell = grid2.getModel().getGridCell(check.cell.x, check.cell.y);
				carNumCell.setCellData("carNum", cell.getData());
				System.out.println("获取到的carNumCell" + cell.getData());
				
			}
		});
		
		//创建分页
		createPageCom(secondComUp);
		
	}
	
	/**
	 * 创建分页
	 * @param com
	 */
	public void createPageCom(Composite com) {
		paginate = new Paginate<FCarInfo>(FCarInfo.class, getContext(), 10);
		
		pagerBar = new PagerBar(com, paginate);
//		//初始化搜索框控件
//		initSearchBar();
//		//初始化分页控件，将全部结果集进行分页处理
		paginateRender();
	}
	
	
	/**
	 * 初始化搜索框控件
	 */
	private void initSearchBar() {
		paginate.addRender(new PaginateRender<FCarInfo>() {

		public void render(List<FCarInfo> records) {
			// 将records拆成很多页，通过起始条目和终止条目
			// initGrid2Data为表格数据显示方法(自己创建)
             // 初始化grid2中的数据
			initGrid2Data(records);

		}

		private void initGrid2Data(List<FCarInfo> records) {
			// TODO Auto-generated method stub
			
		}
	});
		// 将分页控件初始化出来，让其显示到相应的容器中，其父容器通常设置为全布局。
		pagerBar.init();
	}

	/**
	 * 初始化分页控件，将全部结果集进行分页处理
	 */
	private void paginateRender() {
		paginate.addRender(new PaginateRender<FCarInfo>() {

			@Override
			public void render(List<FCarInfo> records) {
				for (FCarInfo fCarInfo : records) {
					
//					System.out.println("获取到的分页数据"+fCarInfo.getCarName());
				}
				
			}

	});
	// 将分页控件初始化出来，让其显示到相应的容器中，其父容器通常设置为全布局。
		pagerBar.init();
	}
		
	private void initGrid2Data(List<FCarInfo> records) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * 新增页面实现
	 * @param secondComDown
	 */
	public void createCarAddCom(Composite secondComDown) {
		// 新增容器实现
		secondComDown.setBorder(cborder);
		GridData carAddGridData = new GridData();
		
		carAddGridData.verticalAlignment = JWT.CENTER;
		carAddGridData.grabExcessVerticalSpace = true;
		GridLayout gridLayout = new GridLayout(2);
		gridLayout.marginLeft=100;
		gridLayout.marginTop=100;
		secondComDown.setLayout(gridLayout);
		secondComDown.setLayoutData(carAddGridData);

		createInputCom(secondComDown);
	}
	/**
	 * 创建文本输入框
	 * @param comMain
	 */
	public void createInputCom(Composite secondComDown) {
		
		//创建文字
		Label label1 = new Label(secondComDown);
		label1.setText("车辆编码：");
		//创建输入框
		carNumText = new Text(secondComDown);
		 
		Label label2 = new Label(secondComDown);
		label2.setText("名称：");
		//创建输入框
		Text text2 = new Text(secondComDown);
		
		Label label3 = new Label(secondComDown);
		label3.setText("车牌号：");
		//创建输入框
		Text text3 = new Text(secondComDown);
		
		Label label4 = new Label(secondComDown);
		label4.setText("购置日期：");
		//创建下拉框
		DatePicker datePicker = new DatePicker(secondComDown);

		
		Label label5 = new Label(secondComDown);
		label5.setText("购买价值：");
		//创建输入框
		Text text5 = new Text(secondComDown);
		
		Label label6 = new Label(secondComDown);
		label6.setText("租赁状态：");
		//创建输入框
		Button check = new Button(secondComDown, JWT.CHECK);
		check.setText("已租赁");
		
	}

}
