package com.jiuqi.cal.liyuanzhuo.ui.car;

import com.jiuqi.dna.ui.wt.UIEntry;
import com.jiuqi.dna.ui.wt.widgets.Shell;

/**
 * 车辆信息新增界面
 * @author 19670
 *
 */
public class CarEntry implements UIEntry{

	@Override
	public void createUI(String[] args, Shell shell) {
		// TODO Auto-generated method stub
		System.out.println("车辆信息页面");
		shell.showPage("CarAddPage");
	}

}
