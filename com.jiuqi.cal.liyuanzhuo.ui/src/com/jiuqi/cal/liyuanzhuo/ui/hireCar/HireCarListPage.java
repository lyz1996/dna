package com.jiuqi.cal.liyuanzhuo.ui.hireCar;

import java.util.List;

import com.jiuqi.cal.liyuanzhuo.ui.hireCar.message.SaveAndUpdateMessage;
import com.jiuqi.carInfo.liyuanzhuo.entity.HireCarEntity;
import com.jiuqi.dna.bap.common.constants.BapImages;
import com.jiuqi.dna.bap.common.paginate.PagerBar;
import com.jiuqi.dna.bap.common.paginate.Paginate;
import com.jiuqi.dna.bap.common.paginate.render.PaginateRender;
import com.jiuqi.dna.ui.common.constants.JWT;
import com.jiuqi.dna.ui.template.launch.TemplateLauncher;
import com.jiuqi.dna.ui.wt.events.ActionEvent;
import com.jiuqi.dna.ui.wt.events.ActionListener;
import com.jiuqi.dna.ui.wt.events.FocusEvent;
import com.jiuqi.dna.ui.wt.events.FocusListener;
import com.jiuqi.dna.ui.wt.graphics.CBorder;
import com.jiuqi.dna.ui.wt.graphics.Color;
import com.jiuqi.dna.ui.wt.graphics.ImageDescriptor;
import com.jiuqi.dna.ui.wt.grid2.Grid2;
import com.jiuqi.dna.ui.wt.grid2.GridModel;
import com.jiuqi.dna.ui.wt.layouts.FillLayout;
import com.jiuqi.dna.ui.wt.layouts.GridData;
import com.jiuqi.dna.ui.wt.layouts.GridLayout;
import com.jiuqi.dna.ui.wt.widgets.Composite;
import com.jiuqi.dna.ui.wt.widgets.Page;
import com.jiuqi.dna.ui.wt.widgets.ToolBar;
import com.jiuqi.dna.ui.wt.widgets.ToolItem;

/**
 * 车辆租赁列表页面
 * @author 19670
 *
 */
public class HireCarListPage extends Page{

	// 创建边框
	private final CBorder cborder = new CBorder(1, JWT.BORDER_STYLE_SOLID, Color.COLOR_SKYBLUE.getColor());
	private Paginate<HireCarEntity> paginate;
	private PagerBar pagerBar;
	
	public HireCarListPage(Composite parent) {
		super(parent);
		// 为页面容器设置GridLayout
		this.setLayout(new FillLayout());
		// 为页面容器设置GridData
		this.setLayoutData(GridData.INS_FILL_BOTH);
		//设置边框
		this.setBorder(cborder);
		// 创建基础容器
		Composite comMain = new Composite(this);
		comMain.setLayout(new GridLayout());
		comMain.setLayoutData(GridData.INS_FILL_HORIZONTAL);
		comMain.setBorder(cborder);
		
		// 创建工具栏
		createToolBar(comMain);
		// 创建列表容器
		createList(comMain);
		// 创建分页
		createPage(comMain);
	}

	/**
	 * 创建工具栏
	 * @param comMain
	 */
	private void createToolBar(Composite comMain) {
		
		// 工具栏容器
		Composite comToolBar = new Composite(comMain);
		comToolBar.setLayout(new GridLayout());
		comToolBar.setLayoutData(GridData.INS_FILL_HORIZONTAL);
		
		//创建工具栏
		ToolBar toolbar = new ToolBar(comToolBar);
		
		//创建工具栏子控件，并放到工具栏
		ToolItem queryItem = new ToolItem(toolbar);
		//修改按钮
		ImageDescriptor queryImage = getContext().find(ImageDescriptor.class, BapImages.ico_application_form_magnify);
		//设置工具栏图标
		queryItem.setImage(queryImage);
		//设置显示名称
		queryItem.setText("查询");
		queryItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent event) {
				TemplateLauncher.openTemplateWindow(HireCarListPage.this,"QueryHirePage",
						JWT.MODAL|JWT.CLOSE,
						JWT.CLOSE|JWT.NONE);

				
			}
		});
		//创建工具栏子控件，并放到工具栏
		ToolItem addItem = new ToolItem(toolbar);
		//新建按钮
		ImageDescriptor addImage = getContext().find(ImageDescriptor.class, BapImages.ico_add);
		//设置工具栏图标
		addItem.setImage(addImage);
		//设置显示名称
		addItem.setText("新建");
		addItem.setSelection(true);
		try {
			addItem.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					TemplateLauncher.openTemplateWindow(HireCarListPage.this,"EditHireCarPage",
							JWT.MODAL|JWT.CLOSE,
							JWT.CLOSE|JWT.NONE);
					
					//发送消息
//				HireCarEntity message = new HireCarEntity();
//				getContext().broadcastMessage(message);
				}
			});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//创建工具栏子控件，并放到工具栏
		ToolItem updateItem = new ToolItem(toolbar);
		//修改按钮
		ImageDescriptor updateImage = getContext().find(ImageDescriptor.class, BapImages.ico_modify);
		//设置工具栏图标
		updateItem.setImage(updateImage);
		//设置显示名称
		updateItem.setText("修改");
		updateItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				TemplateLauncher.openTemplateWindow(HireCarListPage.this,"EditHireCarPage",
						JWT.MODAL|JWT.CLOSE,
						JWT.CLOSE|JWT.NONE);
				
				//发送消息
				HireCarEntity message = new HireCarEntity();
				message.setOrderNo("");
				getContext().broadcastMessage(message);
			}
		});
		
		//创建工具栏子控件，并放到工具栏
		ToolItem saveItem = new ToolItem(toolbar);
		//保存按钮
		ImageDescriptor saveImage = getContext().find(ImageDescriptor.class, BapImages.ico_save);
		//设置工具栏图标
		saveItem.setImage(saveImage);
		//设置显示名称
		saveItem.setText("保存");
		
		//创建工具栏子控件，并放到工具栏
		ToolItem delItem = new ToolItem(toolbar);
		//删除按钮
		ImageDescriptor delImage = getContext().find(ImageDescriptor.class, BapImages.ico_delete);
		//设置工具栏图标
		delItem.setImage(delImage);
		//设置显示名称
		delItem.setText("删除");
		
		//创建工具栏子控件，并放到工具栏
		ToolItem closeItem = new ToolItem(toolbar);
		//关闭按钮
		ImageDescriptor closeImage = getContext().find(ImageDescriptor.class, BapImages.ico_close_window);
		//设置工具栏图标
		closeItem.setImage(closeImage);
		//设置显示名称
		closeItem.setText("关闭");		
		
	}
	
	/**
	 * 创建列表容器
	 * @param comMain
	 */
	private void createList(Composite comMain) {
		
		//创建列表容器
		GridLayout gridLayout = new GridLayout();
		gridLayout.marginLeft=20;
		comMain.setLayout(gridLayout);
		
		comMain.setLayoutData(GridData.INS_FILL_HORIZONTAL);
		comMain.setBorder(cborder);
//		comMain.addClientEventHandler(JWT.CLIENT_EVENT_GRID_SELECTION, name)
		comMain.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		//创建表格
		Grid2 grid2 = new Grid2(comMain);
		grid2.setActiveBorder(cborder);
		
		GridModel gridModel = grid2.getModel();
		gridModel.setColumnCount(9);
		gridModel.setRowCount(12);
		gridModel.setHeaderColumnCount(9);
		gridModel.setHeaderRowCount(10);

		//设置列名
		creatHeaderCloum(gridModel);
		
	}
	
	/**
	 * 分页
	 * @param comMain
	 */
	private void createPage(Composite comMain) {
		paginate = new Paginate<HireCarEntity>(HireCarEntity.class, getContext(), 10);
		Composite pageCom = new Composite(comMain);
		GridLayout gridLayout = new GridLayout();
		gridLayout.marginLeft=650;
		pageCom.setLayout(gridLayout);
		
		pageCom.setLayoutData(GridData.INS_FILL_HORIZONTAL);
		pagerBar = new PagerBar(pageCom, paginate);
		//初始化分页控件，将全部结果集进行分页处理
		paginateRender();
		
	}
	
	/**
	 * 创建列名
	 * @param gridModel
	 */
	public void creatHeaderCloum(GridModel gridModel){
		String[] herderNames = {"行号","订单编号","制单人","出租日期","出租单位","租金","出租时常(月)","车辆","承租人"};
		for (int i = 0; i < herderNames.length; i++) {
			String name = herderNames[i];
			//依次设置列名
			gridModel.getGridCell(i, 0).setShowText(name);
		}
	}

	/**
	 * 初始化分页控件，将全部结果集进行分页处理
	 */
	private void paginateRender() {
		paginate.addRender(new PaginateRender<HireCarEntity>() {

			@Override
			public void render(List<HireCarEntity> records) {
				for (HireCarEntity fCarInfo : records) {
					
				}
				
			}

	});
	// 将分页控件初始化出来，让其显示到相应的容器中，其父容器通常设置为全布局。
		pagerBar.init();
	}
}
