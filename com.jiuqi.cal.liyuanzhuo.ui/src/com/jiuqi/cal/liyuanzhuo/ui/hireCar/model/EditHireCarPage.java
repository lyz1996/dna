package com.jiuqi.cal.liyuanzhuo.ui.hireCar.model;

import com.jiuqi.dna.bap.common.constants.BapImages;
import com.jiuqi.dna.bap.common.control.SearchBar;
import com.jiuqi.dna.ui.common.constants.JWT;
import com.jiuqi.dna.ui.custom.combo.DatePicker;
import com.jiuqi.dna.ui.wt.graphics.CBorder;
import com.jiuqi.dna.ui.wt.graphics.Color;
import com.jiuqi.dna.ui.wt.graphics.ImageDescriptor;
import com.jiuqi.dna.ui.wt.layouts.FillLayout;
import com.jiuqi.dna.ui.wt.layouts.GridData;
import com.jiuqi.dna.ui.wt.layouts.GridLayout;
import com.jiuqi.dna.ui.wt.widgets.Composite;
import com.jiuqi.dna.ui.wt.widgets.Label;
import com.jiuqi.dna.ui.wt.widgets.Page;
import com.jiuqi.dna.ui.wt.widgets.Text;
import com.jiuqi.dna.ui.wt.widgets.ToolBar;
import com.jiuqi.dna.ui.wt.widgets.ToolItem;

/**
 * 车辆租赁编辑页面
 * @author 19670
 *
 */
public class EditHireCarPage extends Page{

	// 创建边框
	private final CBorder cborder = new CBorder(1, JWT.BORDER_STYLE_SOLID, Color.COLOR_SKYBLUE.getColor());

	public EditHireCarPage(Composite parent) {
		super(parent);
		// 为页面容器设置GridLayout
		this.setLayout(new FillLayout());
		// 为页面容器设置GridData
//		this.setLayoutData(GridData.INS_FILL_BOTH);
		//设置边框
		this.setBorder(cborder);
		// 创建基础容器
		Composite comMain = new Composite(this);
		comMain.setWidth(400);
		comMain.setLayout(new GridLayout());
		comMain.setLayoutData(GridData.INS_FILL_HORIZONTAL);
		comMain.setBorder(cborder);
		
		try {
			//构建页面 -- 工具栏构建
			createToolBar(comMain);
//			
//			// 创建数据项
			createData(comMain);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

	/**
	 * 工具栏构建
	 * @param comMain
	 */
	private void createToolBar(Composite comMain) {
		
		// 工具栏容器
		Composite comToolBar = new Composite(comMain);
		comToolBar.setLayout(new GridLayout());
		comToolBar.setLayoutData(GridData.INS_FILL_HORIZONTAL);
		
		//创建工具栏
		ToolBar toolbar = new ToolBar(comToolBar);
		
		//创建工具栏子控件，并放到工具栏
		ToolItem addItem = new ToolItem(toolbar);
		//新建按钮
		ImageDescriptor addImage = getContext().find(ImageDescriptor.class, BapImages.ico_add);
		//设置工具栏图标
		addItem.setImage(addImage);
		//设置显示名称
		addItem.setText("新建");
		addItem.setSelection(true);
		
		
		//创建工具栏子控件，并放到工具栏
		ToolItem updateItem = new ToolItem(toolbar);
		//修改按钮
		ImageDescriptor updateImage = getContext().find(ImageDescriptor.class, BapImages.ico_modify);
		//设置工具栏图标
		updateItem.setImage(updateImage);
		//设置显示名称
		updateItem.setText("修改");

		
		//创建工具栏子控件，并放到工具栏
		ToolItem delItem = new ToolItem(toolbar);
		//删除按钮
		ImageDescriptor delImage = getContext().find(ImageDescriptor.class, BapImages.ico_delete);
		//设置工具栏图标
		delItem.setImage(delImage);
		//设置显示名称
		delItem.setText("删除");
		
		//创建工具栏子控件，并放到工具栏
		ToolItem saveItem = new ToolItem(toolbar);
		//保存按钮
		ImageDescriptor saveImage = getContext().find(ImageDescriptor.class, BapImages.ico_save);
		//设置工具栏图标
		saveItem.setImage(saveImage);
		//设置显示名称
		saveItem.setText("保存");

		//创建工具栏子控件，并放到工具栏
		ToolItem closeItem = new ToolItem(toolbar);
		//关闭按钮
		ImageDescriptor closeImage = getContext().find(ImageDescriptor.class, BapImages.ico_close_window);
		//设置工具栏图标
		closeItem.setImage(closeImage);
		//设置显示名称
		closeItem.setText("关闭");		
		
	}
	

	/**
	 * 新增修改
	 * @param comMain
	 */
	private void createData(Composite comMain) {
		// 获取监听消息判断新增还是修改
//		getContext().regMessageListener(HireCarEntity.class, new MessageListener<HireCarEntity>() {
//			@Override
//			public void onMessage(Situation context, HireCarEntity message,
//					MessageTransmitter<HireCarEntity> transmitter) {
//				// 得到子界面发送消息
//				String messageStr = message.getOrderNo();
//			}
//		});
		
		// 标题栏
		try {
//			Composite titleCom = new Composite(comMain);
//			titleCom.setLayout(new GridLayout(1));
//			titleCom.setLayoutData(GridData.HORIZONTAL_ALIGN_CENTER);
//			

			
			//数据行
			Composite dataCom = new Composite(comMain);
			GridLayout gridLayout = new GridLayout(4);
			gridLayout.marginLeft=30;
			gridLayout.marginTop=50;
			
			dataCom.setLayout(gridLayout);
//			dataCom.setBorder(cborder);
	
			GridData gridData = new GridData();
			gridData.heightHint=300;

//			gridData.grabExcessHorizontalSpace=true;
			dataCom.setLayoutData(gridData);
			

//			new Label(dataCom,JWT.CENTER).setText("车 辆 租 赁 单");
			
			new Label(dataCom).setText("订单编号");
			new Text(dataCom).setRegExp("^/w+$");;
			
			new Label(dataCom).setText("制单人");
			new Text(dataCom).applyMode(1);;
			
			new Label(dataCom).setText("出租日期");
			new DatePicker(dataCom);
			
			new Label(dataCom).setText("租赁单位");
			new Text(dataCom);
			
			new Label(dataCom).setText("承租人");
			new Text(dataCom);
			
			new Label(dataCom).setText("租金");
			new Text(dataCom);
			
			new Label(dataCom).setText("出租时常（月）");
			new Text(dataCom);
			
			new Label(dataCom).setText("车辆");
			new SearchBar(dataCom);
			
			new Label(dataCom).setText("备注");

			GridData remarkGridData = new GridData();
			remarkGridData.horizontalAlignment = JWT.FILL;
			remarkGridData.grabExcessHorizontalSpace = true;
			remarkGridData.horizontalSpan = 3;
			new Text(dataCom,JWT.MULTI).setLayoutData(remarkGridData);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
