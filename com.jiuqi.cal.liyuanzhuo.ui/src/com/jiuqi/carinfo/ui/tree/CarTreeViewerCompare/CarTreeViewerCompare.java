package com.jiuqi.carinfo.ui.tree.CarTreeViewerCompare;

import com.jiuqi.carInfo.liyuanzhuo.entity.FCarInfo;
import com.jiuqi.dna.ui.wt.viewers.IElementComparer;

public class CarTreeViewerCompare implements IElementComparer{

	@Override
	public boolean equals(Object a, Object b) {
		if (a == null || b == null) {
			return false;
		}
		//验证两个节点对象是否是车辆接口类型
		if (a instanceof FCarInfo && b instanceof FCarInfo) {
		    //转换成车辆接口类型并判断是否相等，相等则返回true
			if (((FCarInfo) a).getRecid().equals(
				 ((FCarInfo) b).getRecid())) {
					return true;
				}
			}
		return false;
	}

	@Override
	public int hashCode(Object element) {
		if (element instanceof FCarInfo) {
			return ((FCarInfo) element).getRecid().hashCode();
		}
		return 0;
	}

}
