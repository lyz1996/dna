package com.jiuqi.carinfo.ui.tree.CarTreeViewerProvider;

import java.util.List;

import com.jiuqi.carInfo.liyuanzhuo.entity.FCarInfo;
import com.jiuqi.dna.bap.common.constants.BapImages;
import com.jiuqi.dna.bap.common.constants.Images;
import com.jiuqi.dna.core.situation.Situation;
import com.jiuqi.dna.core.type.GUID;
import com.jiuqi.dna.ui.wt.graphics.FileImageDescriptor;
import com.jiuqi.dna.ui.wt.graphics.ImageDescriptor;
import com.jiuqi.dna.ui.wt.provider.LabelProvider;
import com.jiuqi.dna.ui.wt.provider.TreeContentProvider;

public class CarTreeViewerProvider implements TreeContentProvider, LabelProvider{

	private Situation context;

	public CarTreeViewerProvider (Situation context)
	{
		this.context = context;
	}
	
	@Override
	public Object[] getElements(Object inputElement) {
		// TODO Auto-generated method stub
		if(inputElement==null){
			//当inputElement为空的时候，查找PARENTID字段下为32个0的数据列表作为根节点
			List<FCarInfo> fCarInfos = context.getList(FCarInfo.class,GUID.emptyID);
				return fCarInfos.toArray();
			}else{
			//当inputElement不为空的时候，查找PARENTID字段下为传入节点RECID的数据列表作为根节点
				FCarInfo root = (FCarInfo)inputElement;
				List<FCarInfo> fCarInfos = context.getList(FCarInfo.class,root.getRecid());
				return fCarInfos.toArray();
			}
	}

	@Override
	public ImageDescriptor getImage(Object element) {
		// 如果参数不为null并且是车辆接口类型
		if (element != null && element instanceof FCarInfo) {
			FCarInfo info = (FCarInfo) element;
			// 判断是不是存在子节点
			List<FCarInfo> list = context.getList(FCarInfo.class, info.getRecid());
			// 根节点设置图标
			if (info.getParentid() == GUID.emptyID) {
				ImageDescriptor rootImg = FileImageDescriptor.createImageDescriptor(Images.UI_PLUGIN_NAME,
							Images.IMG_PATH + Images.ICO_TREE_ROOT);
				return rootImg;
			} else if (list != null && list.size() > 0 && info.getParentid() != GUID.emptyID) {
				// 分组节点设置图标
				return context.find(ImageDescriptor.class, BapImages.ico_treenode_function);
		} else {
			// 子节点图标
			return context.find(ImageDescriptor.class, BapImages.ico_treenode_file);
			}
		}
		return null;
	}

	@Override
	public String getText(Object element) {
	    // 如果参数不为null并且是车辆接口类型
		if (element != null && element instanceof FCarInfo) {
			// 返回车辆代码和名称
			return ((FCarInfo) element).getCarCode() + " " + ((FCarInfo) element).getCarName();
		}
		return null;
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		// 判断参数是否为null
		if (null != parentElement) {
			// 判断是不是车辆接口类型
			if (parentElement instanceof FCarInfo) {
				// 转换成车辆接口类型
				FCarInfo fCarInfo = (FCarInfo) parentElement;
				// 查找PARENTID字段为接口RECID的车辆列表
				List<FCarInfo> list = context.getList(FCarInfo.class, fCarInfo.getRecid());
				// 如果查询到的车辆列表不是空的
				if (list != null && list.size() > 0) {
					// 将车辆实体列表转换成FCarInfo类型对象数组并返回
					return list.toArray();
				}
			}
		}
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {
		// 判断参数是否为null
		if (null != element) {
			// 判断是不是车辆接口类型
			if (element instanceof FCarInfo) {
				// 转换成车辆接口类型
				FCarInfo fCarInfo = (FCarInfo) element;
				// 判断是不是存在子节点
				List<FCarInfo> list = context.getList(FCarInfo.class, fCarInfo.getRecid());
				if (list != null && list.size() > 0) {
					// 存在则返回true
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public Object getParent(Object element) {
		// 如果参数不为null，则根据该子节点的PARENTID来查询车辆信息
		if (element != null) {
			// 判断是否是车辆接口类型
			if (element instanceof FCarInfo) {
				// 转换成车辆接口类型
				FCarInfo fCarInfo = (FCarInfo) element;
				if(fCarInfo.getParentid() ==GUID.emptyID||fCarInfo.getParentid().equals(GUID.emptyID)){
					return element;
				}else if (fCarInfo.getParentid() != GUID.emptyID&&fCarInfo!=null) {
					// 根据子节点PARENTID获取父节点数据
					FCarInfo fInfo = context.get(FCarInfo.class,fCarInfo.getParentid());
					return fInfo;
				}
			}
		} else {
			// 如果参数为null，则获取数据库中全部的车辆FCarInfo对象数组
			List<FCarInfo> list = context.getList(FCarInfo.class);
			// 如果该列表不为空
			if (list != null && list.size() > 0) {
				// 默认返回第一个车辆
				return (FCarInfo) list.get(0);
			}
		}
		return null;
	}

}
