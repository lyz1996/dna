package com.jiuqi.carInfo.liyuanzhuo.entity;


import com.jiuqi.dna.core.type.GUID;

public class FCarInfo {
	private String carName;
	private String carTtype;
	private String carCode;
	private GUID recid;
	private GUID parentid;
	
	public String getCarName() {
		return carName;
	}
	public void setCarName(String carName) {
		this.carName = carName;
	}
	public String getCarTtype() {
		return carTtype;
	}
	public void setCarTtype(String carTtype) {
		this.carTtype = carTtype;
	}
	public GUID getRecid() {
		return recid;
	}
	public void setRecid(GUID recid) {
		this.recid = recid;
	}
	
	public GUID getParentid() {
		return parentid;
	}
	public void setParentid(GUID parentid) {
		this.parentid = parentid;
	}
	public String getCarCode() {
		return carCode;
	}
	public void setCarCode(String carCode) {
		this.carCode = carCode;
	}

	
	
}
