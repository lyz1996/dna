package com.jiuqi.carInfo.liyuanzhuo.entity;

import java.math.BigDecimal;
import java.util.Date;

public class HireCarEntity {
	// 订单编号
	private String orderNo;
	// 制单人
	private String orderByPerson;
	// 出租日期
	private Date rentOutDate;
	// 出租单位
	private String rentOutUnit;
	// 租金
	private String rent;
	// 出租时常
	private String rentMonths;
	// 车辆
	private String car;
	// 承租人
	private String rentPerson;
	// 备注
	private String remark;
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getOrderByPerson() {
		return orderByPerson;
	}
	public void setOrderByPerson(String orderByPerson) {
		this.orderByPerson = orderByPerson;
	}
	public Date getRentOutDate() {
		return rentOutDate;
	}
	public void setRentOutDate(Date rentOutDate) {
		this.rentOutDate = rentOutDate;
	}
	public String getRentOutUnit() {
		return rentOutUnit;
	}
	public void setRentOutUnit(String rentOutUnit) {
		this.rentOutUnit = rentOutUnit;
	}
	public String getRent() {
		return rent;
	}
	public void setRent(String rent) {
		this.rent = rent;
	}
	public String getRentMonths() {
		return rentMonths;
	}
	public void setRentMonths(String rentMonths) {
		this.rentMonths = rentMonths;
	}
	public String getCar() {
		return car;
	}
	public void setCar(String car) {
		this.car = car;
	}
	public String getRentPerson() {
		return rentPerson;
	}
	public void setRentPerson(String rentPerson) {
		this.rentPerson = rentPerson;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
}
