package com.jiuqi.carInfo.liyuanzhuo.entity;

import com.jiuqi.dna.core.type.GUID;

public class SearchTreeData {
	
	private GUID recid;

	public GUID getRecid() {
		return recid;
	}

	public void setRecid(GUID recid) {
		this.recid = recid;
	}
	
}
