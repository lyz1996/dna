package com.jiuqi.carInfo.liyuanzhuo.resultType;

import java.util.Date;

import com.jiuqi.dna.core.type.GUID;

public class CarInfoListResultType {
	
	private GUID recid;
	private String carNum;
	
	private String carName;
	private String carType;
	private Date payDate;
	
	public String getCarName() {
		return carName;
	}

	public void setCarName(String carName) {
		this.carName = carName;
	}

	public String getCarNum() {
		return carNum;
	}

	public void setCarNum(String carNum) {
		this.carNum = carNum;
	}

	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public Date getPayDate() {
		return payDate;
	}

	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}

	public GUID getRecid() {
		return recid;
	}

	public void setRecid(GUID recid) {
		this.recid = recid;
	}
	
	
}
