package com.jiuqi.carInfo.liyuanzhuo.service;

import java.util.List;

import com.jiuqi.carInfo.liyuanzhuo.entity.FCarInfo;
import com.jiuqi.carInfo.liyuanzhuo.entity.SearchTreeData;
import com.jiuqi.carInfo.liyuanzhuo.resultType.CarInfoListResultType;
import com.jiuqi.carInfo.liyuanzhuo.task.CreateCarInfoTask;
import com.jiuqi.carInfo.liyuanzhuo.task.DelCarInfoTask;
import com.jiuqi.dna.core.Context;
import com.jiuqi.dna.core.da.DBCommand;
import com.jiuqi.dna.core.da.RecordSet;
import com.jiuqi.dna.core.da.RecordSetField;
import com.jiuqi.dna.core.da.RecordSetFieldContainer;
import com.jiuqi.dna.core.service.Publish;
import com.jiuqi.dna.core.service.Service;
import com.jiuqi.dna.core.situation.Situation;
import com.jiuqi.dna.core.type.GUID;

public class CarInfoService extends Service {

	protected CarInfoService() {
		super("CarInfoService");
		// TODO Auto-generated constructor stub
		System.out.println("CarInfoService已启动！！！！");
	}

	/**
	 * 保存
	 * @author 19670
	 *
	 */
	@Publish
	class CreateCarInfoTaskHandle  extends SimpleTaskMethodHandler<CreateCarInfoTask>{

		@Override
		protected void handle(Context context, CreateCarInfoTask task){
			// TODO Auto-generated method stub
			System.out.println("task info:"+task.getCarName());
			
			String dnaSQL=" define insert createCarInfo(@recid guid,@carNum string) "
					+ "begin "
					+ "insert into CarInfoTB(RECID,CARNUM) values(@recid,@carNum) "
					+ "end ";
			DBCommand dbCommand = null;
			try {
				
				dbCommand = context.prepareStatement(dnaSQL);
				dbCommand.setArgumentValues(GUID.randomID(),task.getCarNum());
				dbCommand.executeUpdate();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				dbCommand.unuse();//释放数据库连接
			}
			
		}
		
	}
	/**
	 * 查询
	 * @author 19670
	 *
	 */
	
	@Publish
	class CarInfoResultHandle extends ResultListProvider<CarInfoListResultType>{

		@Override
		protected void provide(Context context, List<CarInfoListResultType> list) {
			// TODO Auto-generated method stub
			String dnaSQL=" define query carInfoListQuery() "
					+ "begin "
					+ "select c.RECID as recid,c.CARNUM as carNum "
					+ "from CarInfoTB as c "
					+ "end ";
			DBCommand dbCommand = null;
			try {
				dbCommand = context.prepareStatement(dnaSQL);
				RecordSet rs = dbCommand.executeQuery();//获得数据集，二维数据
				CarInfoListResultType resultType;
				while(rs.next()){//根据数据的行数进行循环
					RecordSetFieldContainer<RecordSetField> c = (RecordSetFieldContainer<RecordSetField>) rs.getFields();//获取一行数据
					GUID recid = c.get(0).getGUID();//获取第0列数据
					String carNum = c.get(1).getString();//获取第1列数据
					resultType = new CarInfoListResultType();
					resultType.setRecid(recid);
					resultType.setCarNum(carNum);
					list.add(resultType);
					System.out.println(list.toString());
				}
				
			} catch (IndexOutOfBoundsException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				dbCommand.unuse();//TODO 需要放到finally中
			}
			//结果返回信息add到resultList中，即可完成信息列表的返回
		}
			
	}
	
	/**
	 * 删除
	 * @author 19670
	 *
	 */
	@Publish
	class DelCarInfoTaskHandle  extends SimpleTaskMethodHandler<DelCarInfoTask>{

		@Override
		protected void handle(Context context, DelCarInfoTask task) throws Throwable {
			// TODO Auto-generated method stub
			String dnaSQL="define delete carDelete(@carnum string)"
					+"begin "
					+"delete from CarInfoTB as t "
					+"where t.CARNUM = @carnum "
					+"end ";
			DBCommand dbCommand = null;
			try {
				dbCommand = context.prepareStatement(dnaSQL);
				dbCommand.setArgumentValues(task.getCarNum());
				dbCommand.executeUpdate();				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				dbCommand.unuse();//释放数据库连接
			}
		}
		
	}
	
	/**
	 * 查询树形菜单数据
	 * @author 19670
	 *
	 */
	@Publish
	class QueryTreeTaskHandle  extends OneKeyResultListProvider<FCarInfo,GUID>{

		@Override
		protected void provide(Context context, GUID recids, List<FCarInfo> list) throws Throwable {
			// TODO Auto-generated method stub
			String dnaSQL=" define query QueryTreeData() "
					+ "begin "
					+ " select c.RECID as recid,c.PARENTID as parentid,c.CARNAME as carname,c.CARNUM as carcode "
					+ " from CarInfoTB as c "
					+ "end ";
			DBCommand dbCommand = null;
			FCarInfo resultType = null;
			try {
				dbCommand = context.prepareStatement(dnaSQL);
				RecordSet rs = dbCommand.executeQuery();//获得数据集，二维数据
				while(rs.next()){//根据数据的行数进行循环
					RecordSetFieldContainer<RecordSetField> c = (RecordSetFieldContainer<RecordSetField>) rs.getFields();//获取一行数据
					GUID recid = c.get(0).getGUID();//获取第0列数据
					GUID parentid = c.get(1).getGUID();//获取第1列数据
					String carName = c.get(2).getString();
					String carCode = c.get(3).getString();
					resultType = new FCarInfo();
					resultType.setRecid(recid);
					resultType.setParentid(parentid);
					resultType.setCarName(carName);
					resultType.setCarCode(carCode);
					list.add(resultType);
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				dbCommand.unuse();//TODO 需要放到finally中
			}
			
		}

	}
	
	
	@Publish
	class findTreeData extends OneKeyResultProvider<FCarInfo,SearchTreeData>{

		@Override
		protected FCarInfo provide(Context arg0, SearchTreeData arg1) throws Throwable {
			// TODO Auto-generated method stub
			return null;
		}
		
	}
	
//	@Publish
//	class page extends TwoKeyResultListProvider<FCarInfo, Object, Object>{
//
//		@Override
//		protected void provide(Context arg0, Object arg1, Object arg2, List<FCarInfo> list) throws Throwable {
//			// TODO Auto-generated method stub
//			System.out.println("测试");
//			FCarInfo c = new FCarInfo();
//			list.add(c);
//		}
//		
//	}
	
	/**
	 * 分页结果提供器
	 */
	@Publish
	class PageHelper extends ResultListProvider<FCarInfo>{

		@Override
		protected void provide(Context context, List<FCarInfo> list) throws Throwable {
			// TODO Auto-generated method stub
			String dnaSQL=" define query QueryTreeData() "
					+ "begin "
					+ " select c.RECID as recid,c.PARENTID as parentid,c.CARNAME as carname,c.CARNUM as carcode "
					+ " from CarInfoTB as c "
					+ "end ";
			DBCommand dbCommand = null;
			FCarInfo resultType = null;
			System.out.println("执行分页结果提供器任务");
			try {
				dbCommand = context.prepareStatement(dnaSQL);
				RecordSet rs = dbCommand.executeQuery();//获得数据集，二维数据
				while(rs.next()){//根据数据的行数进行循环
					RecordSetFieldContainer<RecordSetField> c = (RecordSetFieldContainer<RecordSetField>) rs.getFields();//获取一行数据
					GUID recid = c.get(0).getGUID();//获取第0列数据
					GUID parentid = c.get(1).getGUID();//获取第1列数据
					String carName = c.get(2).getString();
					String carCode = c.get(3).getString();
					resultType = new FCarInfo();
					resultType.setRecid(recid);
					resultType.setParentid(parentid);
					resultType.setCarName(carName);
					resultType.setCarCode(carCode);
					list.add(resultType);
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				dbCommand.unuse();//TODO 需要放到finally中
			}
			
		}
		
	}
}
