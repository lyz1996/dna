package com.jiuqi.carInfo.liyuanzhuo.service;

import java.util.List;

import com.jiuqi.carInfo.liyuanzhuo.entity.HireCarEntity;
import com.jiuqi.dna.core.Context;
import com.jiuqi.dna.core.def.query.QueryStatementDeclare;
import com.jiuqi.dna.core.def.table.TableReferenceDeclare;
import com.jiuqi.dna.core.service.Publish;
import com.jiuqi.dna.core.service.Service;

/**
 * 车辆租赁服务启动
 * @author 19670
 *
 */
public class HireCarService extends Service{

	protected HireCarService() {
		super("HireCarEntity");
		// TODO Auto-generated constructor stub
	}

	/**
	 * 车辆租赁列表分页数据提供器
	 * @author 19670
	 *
	 */
	@Publish
	class  QueryHireCarListPageData extends ResultListProvider<HireCarEntity>{

		@Override
		protected void provide(Context context, List<HireCarEntity> list) throws Throwable {
			
			
		}
		
	}
	
	/**
	 * 模糊查询
	 */
	@Publish
	class slurQuery extends OneKeyResultListProvider<HireCarEntity, String>{

		@Override
		protected void provide(Context context, String arg1, List<HireCarEntity> arg2) throws Throwable {
			//模糊查询
			HireCarEntity hireCar = context.get(HireCarEntity.class);
			QueryStatementDeclare query = context.newQueryStatement();
			
			TableReferenceDeclare table = query.newReference(hireCar,"a");
			
		}
		
	}
}
