package com.jiuqi.carInfo.liyuanzhuo.task;

import java.util.Date;

import com.jiuqi.dna.core.invoke.SimpleTask;

public class CreateCarInfoTask extends SimpleTask{
	
	private String carNum;
	
	private String carName;
	private String carType;
	private Date payDate;
	public String getCarNum() {
		return carNum;
	}
	public void setCarNum(String carNum) {
		this.carNum = carNum;
	}
	public String getCarName() {
		return carName;
	}
	public void setCarName(String carName) {
		this.carName = carName;
	}
	public String getCarType() {
		return carType;
	}
	public void setCarType(String carType) {
		this.carType = carType;
	}
	public Date getPayDate() {
		return payDate;
	}
	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}
	
	
	
}
