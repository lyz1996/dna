package com.jiuqi.carInfo.liyuanzhuo.storage;

import com.jiuqi.dna.core.ObjectQuerier;
import com.jiuqi.dna.core.def.arg.ArgumentDefine;
import com.jiuqi.dna.core.def.query.InsertStatementDeclarator;

public final class Insert_CarInfo extends InsertStatementDeclarator {


		public final ArgumentDefine arg_recid;
		public final ArgumentDefine arg_carName;


		public Insert_CarInfo() {
			this.arg_recid = this.statement.getArguments().get(0);
			this.arg_carName = this.statement.getArguments().get(1);
		}
	}
