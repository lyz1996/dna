package com.jiuqi.carInfo.liyuanzhuo.storage;

import com.jiuqi.dna.core.ObjectQuerier;
import com.jiuqi.dna.core.def.query.QueryColumnDefine;
import com.jiuqi.dna.core.def.query.QueryStatementDeclarator;

public final class QueryTreeData extends QueryStatementDeclarator {


		public final QueryColumnDefine c_recid;
		public final QueryColumnDefine c_parentid;
		public final QueryColumnDefine c_carname;
		public final QueryColumnDefine c_carcode;

		public QueryTreeData() {
			this.c_recid = this.query.getColumns().get(0);
			this.c_parentid = this.query.getColumns().get(1);
			this.c_carname = this.query.getColumns().get(2);
			this.c_carcode = this.query.getColumns().get(3);
		}
	}
