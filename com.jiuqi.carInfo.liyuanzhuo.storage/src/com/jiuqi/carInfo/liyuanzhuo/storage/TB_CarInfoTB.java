package com.jiuqi.carInfo.liyuanzhuo.storage;

import com.jiuqi.dna.core.def.table.TableDeclarator;
import com.jiuqi.dna.core.type.TypeFactory;
import com.jiuqi.dna.core.def.table.TableFieldDefine;
import com.jiuqi.dna.core.def.table.TableFieldDeclare;

public final class TB_CarInfoTB extends TableDeclarator {

	public static final String TABLE_NAME ="CarInfoTB";

	public final TableFieldDefine f_carnum;
	public final TableFieldDefine f_carname;
	public final TableFieldDefine f_cartype;
	public final TableFieldDefine f_paydate;
	public final TableFieldDefine f_parentid;

	public static final String FN_carnum ="carnum";
	public static final String FN_carname ="carname";
	public static final String FN_cartype ="cartype";
	public static final String FN_paydate ="paydate";
	public static final String FN_parentid ="parentid";

	//不可调用该构造方法.当前类只能由框架实例化.
	private TB_CarInfoTB() {
		super(TABLE_NAME);
		TableFieldDeclare field;
		this.f_carnum = field = this.table.newField(FN_carnum, TypeFactory.VARCHAR(18));
		field.setDescription("车辆编码");
		this.f_carname = field = this.table.newField(FN_carname, TypeFactory.VARCHAR(18));
		field.setDescription("汽车名称");
		this.f_cartype = field = this.table.newField(FN_cartype, TypeFactory.VARCHAR(32));
		field.setDescription("汽车类型");
		this.f_paydate = field = this.table.newField(FN_paydate, TypeFactory.DATE);
		field.setDescription("购买日期");
		this.f_parentid = this.table.newField(FN_parentid, TypeFactory.GUID);
	}

}
