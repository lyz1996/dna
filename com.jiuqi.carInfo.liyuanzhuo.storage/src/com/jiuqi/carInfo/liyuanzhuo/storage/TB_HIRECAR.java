package com.jiuqi.carInfo.liyuanzhuo.storage;

import com.jiuqi.dna.core.def.table.TableDeclarator;
import com.jiuqi.dna.core.type.TypeFactory;
import com.jiuqi.dna.core.def.table.TableFieldDefine;
import com.jiuqi.dna.core.def.table.TableFieldDeclare;

public final class TB_HIRECAR extends TableDeclarator {

	public static final String TABLE_NAME ="HIRECAR";

	public final TableFieldDefine f_orderno;
	public final TableFieldDefine f_orderByPerson;
	public final TableFieldDefine f_rentOutDate;
	public final TableFieldDefine f_rentOutUnit;
	public final TableFieldDefine f_rent;
	public final TableFieldDefine f_rentMonths;
	public final TableFieldDefine f_car;
	public final TableFieldDefine f_rentPerson;
	public final TableFieldDefine f_remark;

	public static final String FN_orderno ="orderno";
	public static final String FN_orderByPerson ="orderByPerson";
	public static final String FN_rentOutDate ="rentOutDate";
	public static final String FN_rentOutUnit ="rentOutUnit";
	public static final String FN_rent ="rent";
	public static final String FN_rentMonths ="rentMonths";
	public static final String FN_car ="car";
	public static final String FN_rentPerson ="rentPerson";
	public static final String FN_remark ="remark";

	//不可调用该构造方法.当前类只能由框架实例化.
	private TB_HIRECAR() {
		super(TABLE_NAME);
		TableFieldDeclare field;
		this.f_orderno = field = this.table.newField(FN_orderno, TypeFactory.VARCHAR(10));
		field.setDescription("订单编号");
		this.f_orderByPerson = this.table.newField(FN_orderByPerson, TypeFactory.VARCHAR(10));
		this.f_rentOutDate = this.table.newField(FN_rentOutDate, TypeFactory.DATE);
		this.f_rentOutUnit = this.table.newField(FN_rentOutUnit, TypeFactory.VARCHAR(10));
		this.f_rent = this.table.newField(FN_rent, TypeFactory.VARCHAR(10));
		this.f_rentMonths = this.table.newField(FN_rentMonths, TypeFactory.VARCHAR(10));
		this.f_car = this.table.newField(FN_car, TypeFactory.VARCHAR(10));
		this.f_rentPerson = this.table.newField(FN_rentPerson, TypeFactory.VARCHAR(10));
		this.f_remark = this.table.newField(FN_remark, TypeFactory.VARCHAR(10));
	}

}
