package com.jiuqi.carInfo.liyuanzhuo.storage;

import com.jiuqi.dna.core.ObjectQuerier;
import com.jiuqi.dna.core.def.arg.ArgumentDefine;
import com.jiuqi.dna.core.def.query.DeleteStatementDeclarator;

public final class carDelete extends DeleteStatementDeclarator {


		public final ArgumentDefine arg_recid;


		public carDelete() {
			this.arg_recid = this.statement.getArguments().get(0);
		}
	}
