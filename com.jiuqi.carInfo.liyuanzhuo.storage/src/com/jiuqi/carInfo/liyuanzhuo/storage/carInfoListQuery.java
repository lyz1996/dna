package com.jiuqi.carInfo.liyuanzhuo.storage;

import com.jiuqi.dna.core.ObjectQuerier;
import com.jiuqi.dna.core.def.query.QueryColumnDefine;
import com.jiuqi.dna.core.def.query.QueryStatementDeclarator;

public final class carInfoListQuery extends QueryStatementDeclarator {


		public final QueryColumnDefine c_recid;
		public final QueryColumnDefine c_carName;

		public carInfoListQuery() {
			this.c_recid = this.query.getColumns().get(0);
			this.c_carName = this.query.getColumns().get(1);
		}

		private String carName;
		private String carTtype;
		
		public String getCarName() {
			return carName;
		}
		public void setCarName(String carName) {
			this.carName = carName;
		}
		public String getCarTtype() {
			return carTtype;
		}
		public void setCarTtype(String carTtype) {
			this.carTtype = carTtype;
		}

	}
