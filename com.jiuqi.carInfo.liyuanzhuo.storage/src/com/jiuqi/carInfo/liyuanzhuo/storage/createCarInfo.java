package com.jiuqi.carInfo.liyuanzhuo.storage;

import com.jiuqi.dna.core.ObjectQuerier;
import com.jiuqi.dna.core.def.arg.ArgumentDefine;
import com.jiuqi.dna.core.def.query.InsertStatementDeclarator;

public final class createCarInfo extends InsertStatementDeclarator {


		public final ArgumentDefine arg_recid;
		public final ArgumentDefine arg_carName;


		public createCarInfo() {
			this.arg_recid = this.statement.getArguments().get(0);
			this.arg_carName = this.statement.getArguments().get(1);
		}
	}
